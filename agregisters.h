#ifndef AGREGISTERS_H
#define AGREGISTERS_H

#include <QObject>

//#include <QtCore>

enum SyncStatus
{
    REGISTER_SYNCSTAT_UNDEF,
    REGISTER_SYNCSTAT_SYNCRONIZED,
    REGISTER_SYNCSTAT_NEED_READ,
    REGISTER_SYNCSTAT_NEED_WRITE
};

enum Register_ReadWriteType
{
    REGISTER_RWTYPE_READONLY,
    REGISTER_RWTYPE_READWRITE
};

#define Register_RW_ReadOnly 0
#define Register_RW_ReadWrite 1


typedef struct
{
    quint16 RegisterCode; // Код регистра
    QString RegisterName; // Название регистра
    SyncStatus Status; // Статус регистра
    Register_ReadWriteType ReadWriteType; // Доступ на запись
    quint8 Value;   // Значенеи регистра

} RegisterItemType;



class AGRegisters : public QObject
{
    Q_OBJECT
public:
    explicit AGRegisters(QObject *parent = nullptr);
    void InitRegisterList(void); // Наполнение списка регистров
    void AddRegister(quint16 RegisterCode, QString RegisterName, Register_ReadWriteType ReadWriteType, quint8 Value, SyncStatus Status); // Добавление нового регистра
signals:

public slots:

public:
      QList<RegisterItemType>Registers; // Основной массив регстров устройства
};

#endif // AGREGISTERS_H
