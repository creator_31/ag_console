#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "agregisters.h"
#include "registerlistmodel.h"
#include "wid/qsb.h"
#include <QDebug>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    //AGRegisters REG(this);
    REG = new AGRegisters (this);
    REG_MOD = new RegisterListModel(this); // создание экземпляра модели
    mapper = new QDataWidgetMapper(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_pushButton_clicked()
{
    ui->terminal_lines->addItem("UART INITIALIZED AT 38400 8N1");
    ui->terminal_lines->addItem("ptD Start");
    ui->terminal_lines->addItem("GrowOS start complete");
    ui->terminal_lines->addItem("GROWTASK SATART");
}

void MainWindow::on_pushButton_3_clicked()
{
    if(ui->pushButton_3->isChecked())
    {
        ui->pushButton_3->setText("Включено");
    }
    else
    {
        ui->pushButton_3->setText("Отключено");
    }
}

void MainWindow::on_pushButton_4_clicked()
{
    if(ui->pushButton_4->isChecked())
    {
        ui->pushButton_4->setText("Расписание");
    }
    else
    {
        ui->pushButton_4->setText("Отключено");
    }
}

void MainWindow::on_pushButton_5_clicked()
{
  QList<RegisterItemType>::iterator i;
    for (i = REG->Registers.begin(); i != REG->Registers.end(); ++i)
    {
        if(i->RegisterCode == 20056)
        {
            ui->label_4->setText(QString::number(i->Value));

        }

 //       ui->label_3->setText(QString::number(i->RegisterCode));

    }
}

void MainWindow::on_pushButton_6_clicked()
{
    ui->listView->reset();
    ui->listView->setModel(REG_MOD);
    mapper->setModel(REG_MOD);
    mapper->addMapping(ui->label_6,0,"text");
    mapper->setCurrentIndex(2);
    //mapper->setCurrentIndex(0);

}

void MainWindow::on_pushButton_7_clicked()
{
    REG_MOD->dddd();
}
