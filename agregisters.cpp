#include "agregisters.h"

AGRegisters::AGRegisters(QObject *parent) : QObject(parent)
{
    this->InitRegisterList(); // Инициализация регистров
   /* RegisterItemType x;
    x.RegisterCode = 20056;
    x.Value = 150;
    x.Status = REGISTER_SYNCSTAT_UNDEF;
    x.ReadWriteType = REGISTER_RWTYPE_READONLY;

    Registers.append(x);*/
}



void AGRegisters::AddRegister(quint16 RegisterCode, QString RegisterName, Register_ReadWriteType ReadWriteType, quint8 Value, SyncStatus Status) // Добавление нового регистра
{
    RegisterItemType x;
    x.RegisterCode = RegisterCode;
    x.Value = Value;
    x.Status = Status;
    x.ReadWriteType = ReadWriteType;
    x.RegisterName = RegisterName;
    Registers.append(x);
}

void AGRegisters::InitRegisterList(void) // Наполнение списка регистров
{
    AddRegister(10000,"РЕГ 1", REGISTER_RWTYPE_READWRITE,100,REGISTER_SYNCSTAT_NEED_READ);
    AddRegister(20011,"РЕГ 2",REGISTER_RWTYPE_READWRITE,100,REGISTER_SYNCSTAT_NEED_READ);
    AddRegister(20122,"РЕГ 3",REGISTER_RWTYPE_READWRITE,100,REGISTER_SYNCSTAT_NEED_READ);
    AddRegister(33333,"РЕГ 4",REGISTER_RWTYPE_READWRITE,100,REGISTER_SYNCSTAT_NEED_READ);
    AddRegister(20056,"РЕГ 5",REGISTER_RWTYPE_READWRITE,200,REGISTER_SYNCSTAT_NEED_READ);
}
