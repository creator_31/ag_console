#ifndef QSB_H
#define QSB_H

#include <QWidget>

namespace Ui {
class QSB;
}

class QSB : public QWidget
{
    Q_OBJECT

public:
    explicit QSB(QWidget *parent = nullptr);
    ~QSB();

private slots:

private:
    Ui::QSB *ui;
};

#endif // QSB_H
