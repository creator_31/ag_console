#include "agregisters.h"
#include "registerlistmodel.h"

RegisterListModel::RegisterListModel(QObject *parent)
    : QAbstractListModel(parent)
{
    //REG_M. = new AGRegisters (this);
}

QVariant RegisterListModel::headerData(int section, Qt::Orientation orientation, int role) const
{

    // FIXME: Implement me!
}

bool RegisterListModel::setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role)
{
    if (value != headerData(section, orientation, role)) {
        // FIXME: Implement me!
        emit headerDataChanged(orientation, section, section);
        return true;
    }
    return false;
}

int RegisterListModel::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    /*if (parent.isValid())
        return 0;*/
    return REG_M.Registers.count();

    // FIXME: Implement me!
}

QVariant RegisterListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (role == Qt::DisplayRole)
    {
        return REG_M.Registers.at(index.row()).RegisterName;
    }
    else if (role == Qt::EditRole)
    {
        return REG_M.Registers.at(index.row()).RegisterName;
    }
    else
    {
        return QVariant(); //QString("DDD");
    }
}

bool RegisterListModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (data(index, role) != value) {
        // FIXME: Implement me!
        emit dataChanged(index, index, QVector<int>() << role);
        return true;
    }
    return false;
}

Qt::ItemFlags RegisterListModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    return Qt::ItemIsEditable; // FIXME: Implement me!
}

void RegisterListModel::dddd()
{
    REG_M.Registers[2].RegisterName= "SSS DDDD";
    emit dataChanged(index(2,0),index(2,0));
}
