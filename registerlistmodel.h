#ifndef REGISTERLISTMODEL_H
#define REGISTERLISTMODEL_H

#include <QAbstractListModel>
#include "agregisters.h"

class RegisterListModel : public QAbstractListModel
{
    Q_OBJECT

public:
    explicit RegisterListModel(QObject *parent = nullptr);

    // Header:
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

    bool setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role = Qt::EditRole) override;

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    // Editable:
    bool setData(const QModelIndex &index, const QVariant &value,
                 int role = Qt::EditRole) override;

    Qt::ItemFlags flags(const QModelIndex& index) const override;
    void dddd();

private:
     AGRegisters REG_M; // Хранилище данных
};

#endif // REGISTERLISTMODEL_H
